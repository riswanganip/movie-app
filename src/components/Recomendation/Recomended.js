import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import React, {useEffect, useState} from 'react';

import axios from 'axios';

const Recomended = ({navigation}) => {
  const [movies, setMovies] = useState([]);

  const getData = async () => {
    try {
      const res = await axios.get('http://code.aldipee.com/api/v1/movies');
      setMovies(res.data.results);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getData();
  });

  return (
    <View>
      <View style={{marginTop: 10}}>
        <Text style={styles.text}>Recommended</Text>
      </View>
      <ScrollView
        style={{flexGrow: 1}}
        horizontal
        showsHorizontalScrollIndicator={false}>
        {movies &&
          movies.map(item => (
            <TouchableOpacity
              key={item.id}
              style={styles.coverImg}
              onPress={() => navigation.navigate('Detail', {data: item})}>
              <View style={{padding: 5}}>
                <Image style={styles.img} source={{uri: item.poster_path}} />
              </View>
            </TouchableOpacity>
          ))}
      </ScrollView>
    </View>
  );
};

// () => navigation.navigate('Detail', {data: item.id})

export default Recomended;

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  text: {
    marginHorizontal: 10,
    color: 'white',
    fontWeight: '600',
    fontSize: 18,
  },
  coverImg: {
    marginVertical: 10,
    marginHorizontal: 5,
    width: windowWidth * 0.4,
    height: windowHeight * 0.3,
    borderRadius: 10,
  },
  img: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
    borderRadius: 10,
  },
});
