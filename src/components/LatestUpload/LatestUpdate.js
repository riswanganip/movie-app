import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

import React, {useEffect, useState} from 'react';
import axios from 'axios';

function LatestUpdate({navigation}) {
  const [movies, setMovies] = useState([]);

  const getData = async () => {
    try {
      const res = await axios.get('http://code.aldipee.com/api/v1/movies');
      setMovies(res.data.results);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <View style={{marginTop: 10}}>
        <Text style={styles.textUpdate}>Last Updated</Text>
      </View>
      <ScrollView style={{flexGrow: 1}}>
        {movies &&
          movies.map((item, index) => (
            <View style={styles.card} key={index}>
              <View style={styles.cover}>
                <Image style={styles.img} source={{uri: item.poster_path}} />
              </View>
              <View style={styles.desc}>
                <View>
                  <Text style={styles.title}>{item.original_title}</Text>
                  <Text style={styles.ket}>{item.release_date}</Text>
                  <Text style={styles.ket}>Animation</Text>
                </View>
                <TouchableOpacity
                  onPress={() => navigation.navigate('Detail', {data: item})}
                  style={styles.appButtonContainer}>
                  <Text style={styles.appButtonText}>ShowMore</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))}
      </ScrollView>
    </>
  );
}

export default LatestUpdate;

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  textUpdate: {
    marginHorizontal: 10,
    marginVertical: 10,
    color: 'white',
    fontWeight: '600',
    fontSize: 18,
  },
  card: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  cover: {
    marginRightVertical: 15,

    marginVertical: 10,
    marginHorizontal: 10,
    width: windowWidth * 0.4,
    height: windowHeight * 0.3,
    borderRadius: 10,
  },
  img: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
    borderRadius: 10,
  },
  desc: {
    marginHorizontal: 10,
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  title: {
    fontSize: 18,
    color: '#fff',
    marginVertical: 3,
  },
  ket: {
    fontSize: 16,
    color: '#fff',
  },
  appButtonContainer: {
    marginVertical: 5,
    elevation: 8,
    backgroundColor: '#E82626',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  appButtonText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
});
