import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';

import Share from 'react-native-share';
import Icon from 'react-native-vector-icons/Ionicons';

import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {star} from '../../assets/images';

const DetailMovie = ({route, navigation}) => {
  const id = route.params.data.id;

  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);

  const shareDetail = async () => {
    const shareOptions = {
      message: 'Share message test!',
    };

    try {
      const shareResponse = await Share.open(shareOptions);
    } catch (error) {
      console.log('Error =>', error);
    }
  };

  const getData = async () => {
    try {
      const res = await axios.get(
        `http://code.aldipee.com/api/v1/movies/${id}`,
      );
      setMovies(res.data);
      setLoading(true);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const components = (
    <ScrollView style={{flexGrow: 1}}>
      <View style={{flex: 1, position: 'absolute'}}>
        <Image source={{uri: movies.backdrop_path}} style={styles.header} />
      </View>

      <View style={styles.contianerAllDesc}>
        <View style={styles.container}>
          <View style={styles.imgCover}>
            <Image style={styles.img} source={{uri: movies.poster_path}} />
          </View>
          <View style={styles.detailImage}>
            <View style={styles.liteDetail}>
              <Text style={styles.title}>{movies.original_title}</Text>
              <Text style={styles.text}>{movies.tagline}</Text>
              <Text style={styles.text}>{movies.status}</Text>
            </View>
            <View style={styles.tag}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <Image source={star} />
                <Text style={styles.textTag}>{movies.vote_average}</Text>
              </View>
              <View>
                <Text style={styles.textTag}>Runtime: {movies.runtime}</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={{marginHorizontal: 20, marginVertical: 10}}>
          <View>
            <Text style={{fontWeight: '600', fontSize: 16, color: 'white'}}>
              Genre
            </Text>
          </View>
          <View style={styles.coverGenres}>
            {movies.genres &&
              movies.genres.map((item, i) => (
                <View style={styles.genres} key={i}>
                  <Text style={styles.textTags}>{item.name}</Text>
                </View>
              ))}
          </View>
        </View>

        <View style={{marginHorizontal: 20}}>
          <View>
            <Text style={styles.titleAll}>Synopsis</Text>
            <Text style={styles.sinopsis}>{movies.overview}</Text>
          </View>
        </View>

        <View style={{marginHorizontal: 20}}>
          <View style={{marginVertical: 10}}>
            <Text style={styles.titleAll}>Actrees/Actors</Text>
          </View>

          <View style={styles.wrapActrees}>
            {movies.credits &&
              movies.credits.cast.map((item, index) => {
                return (
                  <View key={index} style={styles.actreesImg}>
                    <View style={{width: '100%', height: 100}}>
                      <Image
                        style={{
                          resizeMode: 'stretch',
                          width: '100%',
                          height: '100%',
                          borderRadius: 10,
                        }}
                        source={{uri: item.profile_path}}
                      />
                    </View>
                    <View>
                      <Text style={styles.sinopsis}>{item.original_name}</Text>
                    </View>
                  </View>
                );
              })}
          </View>
        </View>
      </View>
    </ScrollView>
  );

  const loadingComponents = (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size={50} color="white" />
      <Text style={{color: 'white', fontSize: 15, fontWeight: '600'}}>
        Sedang mengambil data
      </Text>
    </View>
  );

  return (
    <View>
      <View style={styles.toggle}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="ios-chevron-back" size={30} color="white" />
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity onPress={shareDetail}>
            <Icon name="share-social" size={30} color="white" />
          </TouchableOpacity>
        </View>
      </View>

      {loading ? components : loadingComponents}
    </View>
  );
};

export default DetailMovie;

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  contianerAllDesc: {
    position: 'relative',
    marginTop: windowHeight * 0.13,
  },
  toggle: {
    position: 'relative',
    flexDirection: 'row',
    marginVertical: 20,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  header: {
    position: 'absolute',
    width: windowWidth,
    height: windowHeight * 0.35,
    opacity: 0.15,
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
    borderRadius: 10,
    justifyContent: 'space-evenly',
  },
  imgCover: {
    flex: 1,
    marginRight: 30,
    width: windowWidth * 0.3,
    height: windowHeight * 0.23,
    borderRadius: 10,
    resizeMode: 'contain',
  },
  detailImage: {
    flex: 1.5,
  },
  img: {
    width: '100%',
    height: '100%',
    resizeMode: 'stretch',
    borderRadius: 10,
  },
  liteDetail: {
    justifyContent: 'flex-start',
  },
  tag: {
    flexDirection: 'row',
    marginVertical: 20,
    justifyContent: 'space-around',
  },
  tagContent: {
    width: 55,
    backgroundColor: '#E82626',
    padding: 5,
    borderRadius: 5,
  },
  title: {
    color: 'white',
    fontWeight: '600',
    fontSize: 21,
    marginBottom: 2,
  },
  titleAll: {
    marginBottom: 3,
    fontWeight: '600',
    fontSize: 16,
    color: 'white',
  },
  text: {
    fontWeight: '400',
    fontSize: 13,
    color: 'white',
  },
  textTag: {
    fontWeight: '600',
    color: 'white',
    fontSize: 14,
  },
  coverGenres: {
    flexDirection: 'row',
  },
  genres: {
    marginRight: 10,
    marginTop: 5,
    borderRadius: 5,
    backgroundColor: '#E82626',
  },
  textTags: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
    fontWeight: '600',
    color: 'white',
    fontSize: 12,
  },
  sinopsis: {
    color: 'white',
    fontSize: 14,
  },
  actreesImg: {
    flexDirection: 'column',
    width: 100,
    height: 150,
    alignItems: 'center',
  },
  wrapActrees: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
