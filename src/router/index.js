/* eslint-disable react/function-component-definition */
/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../pages/Home';
import Splash from '../pages/Splash';
import Detail from '../pages/Detail';

const Stack = createNativeStackNavigator();

const Router = () => (
  <Stack.Navigator initialRouteName="Splash">
    <Stack.Screen
      name="Splash"
      component={Splash}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Home"
      component={Home}
      options={{headerShown: false, title: 'Home'}}
    />
    <Stack.Screen
      name="Detail"
      component={Detail}
      options={{headerShown: false, title: 'Detail'}}
    />
  </Stack.Navigator>
);

export default Router;
