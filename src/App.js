/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/function-component-definition */
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';

const App = () => (
  <NavigationContainer>
    <Router />
  </NavigationContainer>
);

export default App;
