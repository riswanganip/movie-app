import {
  StyleSheet, Image, ImageBackground,
} from 'react-native';
import React, { useEffect } from 'react';

import { backgroundImage, logoBig as movieLogo } from '../assets/images';

function Splash({ navigation }) {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    }, 3000);
  }, [navigation]);

  return (
    <ImageBackground source={backgroundImage} style={styles.background}>
      <Image source={movieLogo} style={styles.logo} />
    </ImageBackground>
  );
}

export default Splash;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
