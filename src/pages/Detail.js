import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';

import React from 'react';

import DetailMovie from '../components/Detail/DetailMovie';
import {COLOR_BACKGROUND1} from '../utils/constans';

const Detail = ({route, navigation}) => {
  return (
    <SafeAreaView style={{flexGrow: 1, backgroundColor: COLOR_BACKGROUND1}}>
      <DetailMovie navigation={navigation} route={route} />
    </SafeAreaView>
  );
};
export default Detail;

const styles = StyleSheet.create({});
