import React, {useEffect, useState} from 'react';
import axios from 'axios';

import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

import LatestUpdate from '../components/LatestUpload/LatestUpdate';
import Recomended from '../components/Recomendation/Recomended';

import {COLOR_BACKGROUND1} from '../utils/constans';

function Home({navigation}) {
  return (
    <SafeAreaView style={{flexGrow: 1, backgroundColor: COLOR_BACKGROUND1}}>
      <Recomended navigation={navigation} />
      <LatestUpdate navigation={navigation} />
    </SafeAreaView>
  );
}

export default Home;

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

// const styles = StyleSheet.create({
//   textUpdate: {
//     marginHorizontal: 10,
//     marginVertical: 10,
//     color: 'white',
//     fontWeight: '600',
//     fontSize: 18,
//   },
//   card: {
//     flex: 1,
//     flexDirection: 'row',
//     padding: 10,
//   },
//   cover: {
//     marginRightVertical: 15,

//     marginVertical: 10,
//     marginHorizontal: 10,
//     width: windowWidth * 0.4,
//     height: windowHeight * 0.3,
//     borderRadius: 10,
//   },
//   img: {
//     width: '100%',
//     height: '100%',
//     resizeMode: 'stretch',
//     borderRadius: 10,
//   },
//   desc: {
//     marginHorizontal: 10,
//     flex: 1,
//     justifyContent: 'space-around',
//     alignItems: 'flex-start',
//   },
//   title: {
//     fontSize: 18,
//     color: '#fff',
//     marginVertical: 3,
//   },
//   ket: {
//     fontSize: 16,
//     color: '#fff',
//   },
//   appButtonContainer: {
//     marginVertical: 5,
//     elevation: 8,
//     backgroundColor: '#E82626',
//     borderRadius: 10,
//     paddingVertical: 10,
//     paddingHorizontal: 12,
//   },
//   appButtonText: {
//     fontSize: 18,
//     color: '#fff',
//     fontWeight: 'bold',
//     alignSelf: 'center',
//     textTransform: 'uppercase',
//   },
//   text: {
//     marginHorizontal: 10,
//     color: 'white',
//     fontWeight: '600',
//     fontSize: 18,
//   },
//   coverImg: {
//     // flexDirection: 'row',
//     marginVertical: 10,
//     marginHorizontal: 5,
//     width: windowWidth * 0.4,
//     height: windowHeight * 0.3,
//     borderRadius: 10,
//   },
//   img: {
//     width: '100%',
//     height: '100%',
//     resizeMode: 'stretch',
//     borderRadius: 10,
//   },
// });
