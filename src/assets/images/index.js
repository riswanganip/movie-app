const backgroundImage = require('./backgroundSplash.png');
const logo = require('./logo.png');
const logoBig = require('./movieLogo.png');
const fatman = require('./fatman.png');
const g3d = require('./3d.png');
const star = require('./star.png');

export {
  backgroundImage, logo, logoBig, fatman, g3d,star
};
